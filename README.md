# Creating a Sentiment Analysis Web App Using Pytorch and AWS SageMaker

The goal of this project is to use Amazon SageMaker to analyze IMDB movie review data to create sentiment analysis model, deploy the model, and access the deployed model using publicly accessible API and a simple web page which interacts with the deployed endpoint.

### Overall flow

The overall flow consists of the following steps:  

1. Download the data
2. Prepare and process the data
    * Remove HTML tags
    * Convert to lower case
    * Split string to words
    * Remove stop words
    * Apply stemming
    * Create a word dictionary (vocabulary)
    * Save the word dictionary
    * Transform the reviews using the dictionary
3. Upload the processed data to S3
4. Build and train the PyTorch Model
5. Test the model
6. Deploy the model for the web app
7. Use the model for the web app
8. Deploy the web app
    * Setup Lambda function
    * Setup API gateway.  
    
### Cloud computing    
    
Cloud computing can simply be thought of as transforming an Information Technology (IT) product into a service. Generally, think of cloud computing as using an internet connected device to log into a cloud computing service, like Google Drive, to access an IT resource, your vacation photos. These IT resources, your vacation photos, are stored in the cloud provider’s data center. Besides cloud storage, other cloud services include: cloud applications, databases, virtual machines, and other services like SageMaker.

Figure below depicts the overall machine learning workflow.

![Figure 1: ML workflow](figures/ML_workflow.jpg)

There are three primary methods used to transfer a model from the modeling component to the deployment component of the machine learning workflow.

* Python model is recoded into the programming language of the production environment.
 
* Model is coded in Predictive Model Markup Language (PMML) or Portable Format Analytics (PFA).
 
* Python model is converted into a format that can be used in the production environment.

The third method that's most similar to what’s used for deployment within Amazon’s SageMaker.

The third method is to build a Python model and use libraries and methods that convert the model into code that can be used in the production environment. Specifically most popular machine learning software frameworks, like PyTorch, TensorFlow, SciKit-Learn, have methods that will convert Python models into intermediate standard format, like ONNX (Open Neural Network Exchange format). This intermediate standard format then can be converted into the software native to the production environment.

This is the easiest and fastest way to move a Python model from modeling directly to deployment. Moving forward this is typically the way models are moved into the production environment. Technologies like containers, endpoints, and APIs (Application Programming Interfaces) also help ease the work required for deploying a model into the production environment.


### Endpoint

Figure below depicts a usage of endpoint which is an interface to the model. This interface (endpoint) facilitates an ease of communication between the model and the application. Specifically, this interface (endpoint) allows the application to send user data to the model, and
receives predictions back from the model based upon that user data.

![Figure 2: Endpoint](figures/Model_app_endpoint.jpg)

We would like to deploy the model and access the model for any input submitted through an application (app). However, there are two problems:
1. our models endpoint expects integer encoded version of the review, where as the app accepts a string, and
2. a deployed sagemaker object will allow access to another object which is authenticated with AWS and has permission to use a sagemaker endpoint.

The way that we are going to approach solving these issues is by making use of Amazon Lambda and API Gateway.

### Lambda function

In general, a Lambda function is an example of a 'Function as a Service'. It lets you perform actions in response to certain events, called triggers. Essentially, you get to describe some events that you care about, and when those events occur, your code is executed.

For example, you could set up a trigger so that whenever data is uploaded to a particular S3 bucket, a Lambda function is executed to process that data and insert it into a database somewhere.

One of the big advantages to Lambda functions is that since the amount of code that can be contained in a Lambda function is relatively small, you are only charged for the number of executions.

In our case, the Lambda function we are creating is meant to process user input and interact with our deployed model. Also, the trigger that we will be using is the endpoint that we will create using API Gateway.

### API gateway

What we need to do now is set up some way to send our user data to the Lambda function.

The way that we will do this is using a service called API Gateway. Essentially, API Gateway allows us to create an HTTP endpoint (a web address). In addition, we can set up what we want to happen when someone tries to send data to our constructed endpoint.

In our application, we want to set it up so that when data is sent to our endpoint, we trigger the Lambda function that we created earlier, making sure to send the data to our Lambda function for processing. Then, once the Lambda function has retrieved the inference results from our model, we return the results back to the original caller.    

### Web app
The structure for our web app will look like the diagram below depicted below.

![Figure 3: Web application](figures/Model_deployment.jpg)

* To begin with, a user will type out a review and enter it into our web app.
* Then, our web app will send that review to an endpoint that we created using API Gateway. This endpoint will be constructed so that anyone (including our web app) can use it.
* API Gateway will forward the data on to the Lambda function
* Once the Lambda function receives the user's review, it will process that review by tokenizing it and then creating a bag of words encoding of the review. After that, it will send the processed review off to our deployed model.
* Once the deployed model performs inference on the processed review, the resulting sentiment will be returned back to the Lambda function.
* Our Lambda function will then return the sentiment result back to our web app using the endpoint that was constructed using API Gateway.

The following figures illustrate the results obtained using the web app for a positive and a negative review.

![Figure 4: Positive review](figures/Positive_review.JPG)

![Figure 5: Negative review](figures/Negative_review.JPG)


## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/sagemaker-deployment).


## Dependencies

The information regarding dependencies for the project can be obtained from [here](https://github.com/udacity/sagemaker-deployment).

## Detailed Writeup

Detailed report can be found in [_Sentiment_analysis_deployment.ipynb_](./Sentiment_analysis_deployment.ipynb).

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

